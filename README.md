# zettel-rust

## How to run?
0. First get in the devcontainer.
1. Each directory with a .toml file is an example.
2. Each example can be run with a `cargo run -p {DIR_NAME} --target {YOUR_TARGET}`
3. Example: `cargo run -p flappy --target x86_64-pc-windows-gnu` (in this case, an .exe file will be created, so you might as well use `build` instead of `run`)


## Compilation targets
1. Web/wasm:
```
make run d=flappy
```
2. Windows: 
```
cargo build -p flappy --target x86_64-pc-windows-gnu
```

## Troubleshooting

```
error: failed to open `/usr/local/cargo/registry/cache/github.com-1ecc6299db9ec823/bytemuck-1.12.1.crate`

Caused by:
  Permission denied (os error 13)
make: *** [Makefile:2: run] Error 101
```

Fix:
```
sudo chown -R $(whoami) /usr/local/cargo/
```