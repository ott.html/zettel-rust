run: 
	cargo build -p $(d) --target wasm32-unknown-unknown
	make create_wasm_dir
	wasm-bindgen target/wasm32-unknown-unknown/debug/$(d).wasm --out-dir wasm --out-name index --no-modules --no-typescript
	python3 -m http.server 8000 --directory ./wasm

create_wasm_dir:
	mkdir -p wasm
	@echo "$$INDEX_FILE" > ./wasm/index.html

define INDEX_FILE
<html>
  <head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
  </head>
  <body>
    <canvas id="canvas" width="640" height="480"></canvas>
    <script src="./index.js"></script>
    <script>
      window.addEventListener("load", async () => {
        await wasm_bindgen("./index_bg.wasm");
      });
    </script>
  </body>
</html>
endef
export INDEX_FILE