use crate::http::Request;
use std::{convert::TryFrom, io::Read, net::TcpListener};

pub struct Server {
    addr: String
}

impl Server {
    pub fn new(addr: String) -> Self {
        Self { addr }
    }

    pub fn run(self) {
        println!("Listening on {:?}", self.addr);

        let listener = TcpListener::bind(&self.addr).unwrap();

        loop {
            match listener.accept() {
                Err(e) => { println!("Failed to establish a connection: {:?}", e); }
                Ok((mut stream, addr)) => {
                    println!("Connection established! {:?}", addr);
                    let mut buffer = [0; 1024];
                    match stream.read(&mut buffer) {
                        Ok(_) => {
                            print!("{}", String::from_utf8_lossy(&buffer));
                            match Request::try_from(&buffer[..]) {
                                Ok(_request) => {}
                                Err(e) => { println!("Failed to parse a request: {:?}", e); }
                            }
                        }
                        Err(e) => {
                            println!("Failed to read from connection: {:?}", e);
                        }
                    }
                }
            }
        }
    }
}