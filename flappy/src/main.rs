use bracket_lib::prelude::*;

fn main() -> BError {
    let context = BTermBuilder::simple80x50()
        .with_title("Flappy Dragon")
        .build()?;

    main_loop(context, State::new())
}

enum GameMode {
    Menu,
    Playing,
    // End,
}

struct State {
    mode: GameMode,
}

impl State {
    fn new() -> Self {
        Self {
            mode: GameMode::Menu,
        }
    }

    fn play(&mut self, _: &mut BTerm) {
        // TODO: Play the game
        self.mode = GameMode::Playing;
    }

    fn main_menu(&mut self, ctx: &mut BTerm) {
        ctx.cls();
        ctx.print_centered(5, "Welcome to Flappy Dragon");
        ctx.print_centered(8, "Press [P] to start");
        ctx.print_centered(9, "Press [Q] to quit");

        if let Some(key) = ctx.key {
            match key {
                VirtualKeyCode::P => self.play(ctx),
                VirtualKeyCode::Q => ctx.quitting = true,
                _ => {}
            }
        }
    }

    // fn dead(&mut self, ctx: &mut BTerm) {
    //     ctx.cls();
    //     ctx.print_centered(5, "You died!");
    //     ctx.print_centered(8, "Press [P] to start");
    //     ctx.print_centered(9, "Press [Q] to quit");

    //     if let Some(key) = ctx.key {
    //         match key {
    //             VirtualKeyCode::P => self.play(ctx),
    //             VirtualKeyCode::Q => ctx.quitting = true,
    //             _ => {}
    //         }
    //     }

    // }

    // fn restart(&mut self) {
    //     self.mode = GameMode::Playing;
    // }
}

impl GameState for State {
    fn tick(&mut self, ctx: &mut BTerm) {
        match self.mode {
            GameMode::Menu => self.main_menu(ctx),
            // GameMode::End => self.dead(ctx),
            GameMode::Playing => self.play(ctx),
        }
    }
}