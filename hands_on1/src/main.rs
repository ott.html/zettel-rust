use std::io::stdin;

fn main() {
    let mut visitors = vec![
        Visitor::new("octavio".to_string(), VisitorAction::Accept, 10),
        Visitor::new("tavin".to_string(), VisitorAction::AcceptWithNote {
            note: "keep an eye on him".to_string() 
        }, 10),
        Visitor::new("octa".to_string(), VisitorAction::Refuse, 10),
    ];
    loop {
        println!("Name?");
        let name = get_name();

        let known_visitor = visitors
            .iter()
            .find(|visitor| visitor.name == name);

        match known_visitor {
            Some(visitor) => visitor.greet(),
            None => {
                if name.is_empty() {
                    break;
                } else {
                    println!("You weren't invited, but I'll add you");
                    visitors.push(
                        Visitor::new(
                            name.to_string(),
                            VisitorAction::Probation, 
                            0
                        )
                    );

                }
            }
        }
    }
    println!("{:#?}", visitors);
}

fn get_name() -> String {
    let mut name = String::new();
    stdin().read_line(&mut name).expect("Failed to read input");
    name.trim().to_string()
}

#[derive(Debug)]
struct Visitor {
    name: String,
    action: VisitorAction,
    age: u8,
}

impl Visitor {
    fn new(name: String, action: VisitorAction, age: u8) -> Self {
        Self {
            name,
            action,
            age,
        }
    }

    fn greet(&self) {
        match &self.action {
            VisitorAction::Accept => println!("welcome {}", self.name),
            VisitorAction::AcceptWithNote { note } => {
                println!("welcome {}. Here's your note: {}", self.name, note);
                if self.age < 21 {
                    println!("do not serve alcohol to {}", self.name)
                }
            },
            VisitorAction::Probation => println!("{} is now a probationary member", self.name),
            VisitorAction::Refuse => println!("{} is banned!", self.name),
        }
    }
}

#[derive(Debug)]
enum VisitorAction {
    Accept,
    AcceptWithNote { note: String },
    Refuse,
    Probation,
}